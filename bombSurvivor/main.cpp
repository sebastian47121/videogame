#include "mainwindow.h"
#include <QApplication>
#include <QTimer>
#include <QSplashScreen>

int main(int argc, char *argv[])
{
    //Se coloca imagen al ejecutar el programa
    QApplication a(argc, argv);
    QSplashScreen *Splash=new QSplashScreen;
    Splash->setPixmap(QPixmap(":/images/logoGame.png"));
    Splash->show(); //Se muestra dicha imagen
    MainWindow w;
    QTimer::singleShot(3000,Splash,SLOT(close()));
    QTimer::singleShot(3000,&w,SLOT(show()));
    return a.exec();
}
