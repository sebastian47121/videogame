#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsLineItem>
#include <QKeyEvent>
#include <QGraphicsView>
#include <QTimer>
#include "bomba.h"
#include "perks.h"
#include "personaje.h"
#include "personajegraf.h"
#include "enemy.h"
#include <QMediaPlayer>
#include <map>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

using namespace std;

namespace Ui {
class game;
}

class game : public QWidget
{
    Q_OBJECT

public:
    explicit game(QWidget *parent = nullptr);
    ~game();
public slots:
    void animar();
    void animar_perk();
    void cargarDatos(map <string,int> mapaDatos);
    void game_over();
    void start();
    void saltar();
    void moverObjetos();
    void leftMove();
    void rightMove();
    void jumpMove();
    void Joy();
private slots:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    void on_connect_clicked();


private:
    Ui::game *ui;
    QGraphicsScene* scene;

    QGraphicsLineItem* l2;
    QGraphicsLineItem* l3;
    QGraphicsLineItem* l4;

    QList<bomba*> bolas;
    QList<perks*> perk;
    QList<bomba*> lateral;

    int puntaje=0;
    int agregar = 3000, tiempo = 0, tiempo2=0, agregarLateral = 2000;

    int vel;
    double dt = 0.1;
    bool loadScore=false;
    bool vidas = false;
    int cont=0, contBegin=0, tiempoLateral = 0;
    bool flag = false;

    string username1;
    map <string,int> datosUsuario;
    map <char,bool> movimiento;


    personajegraf *c;
    enemy *enemigo;

    QTimer *timer;
    QTimer *timer_salto;
    QTimer *timer_mov;
    QTimer *timer_par;
    QTimer *timer_lifes;
    QTimer *timer_gover;
    QTimer *timer_begin;
    QTimer *timer_mapMov;

    QMediaPlayer *effect;

    QTimer *timer_control;
    QSerialPort *control;

    bool keyGeneral=false;
};

#endif // GAME_H
