#include "personaje.h"

personaje::personaje()
{
    px=0;
    py=0;
    vy=50;
    G=-10;
}
void personaje::set_valores(float x, float y)
{
  px=x;
  py=y;
}
float personaje::get_px()
{
  return px;
}
float personaje::get_py()
{
  return py;
}
void personaje::set_px(float x)
{
  px=x;
}
void personaje::set_py(float y)
{
    py=y;
}

void personaje::perderVida()
{
    vidas-=1;
}
void personaje::jump(){

    tocarSuelo=false;
    py = py + vy*dt +G*dt*dt/2;
    vy = vy + G*dt;
    if(py<0){
        py=0;
        tocarSuelo=true;
    }

}
void personaje::ganarVida(){
    vidas+=1;
}
