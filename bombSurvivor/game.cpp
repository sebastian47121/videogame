#include "game.h"
#include "ui_game.h"
#include "mainwindow.h"
#include <QDebug>
#include <random>
#include <QMessageBox>
#include <fstream>
#include <random>

#include<QMenuBar>
#include<QStatusBar>

using namespace std;
void cargarDatos(map <string,int> &mapaDatos);

game::game(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::game)
{
    ui->setupUi(this);
    //Se coloca el icono al boton que conecta el Joystick del Arduino
    QSize size(30,30);
    QPixmap pixmap(":/images/joystick.png");
    QIcon ButtonIcon(pixmap);
    ui->connect->setIcon(ButtonIcon);
    ui->connect->setIconSize(size);
    //Timer que controla el Joystick del Arduino
    timer_control=new QTimer;
    control = new QSerialPort(this);
    connect(timer_control,SIGNAL(timeout()),this,SLOT(Joy()));

    //Elimina el Scrollbar de la ventana
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setFixedSize(1000,500);
    scene=new QGraphicsScene(0,0,1000,500);
    //Agrega el graphicsView a la escena y pone una imagen de fondo
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setBackgroundBrush(QImage(":/images/city.jpg"));
    //Coloca el movimiento del personaje en falso
    movimiento.emplace('r',false);
    movimiento.emplace('l',false);
    movimiento.emplace('j',false);
    //Temporizador que controla las animaciones de los personajes y las bombas
    timer=new QTimer(this);
    timer->stop();
    connect(timer,SIGNAL(timeout()),this,SLOT(animar()));
    //Temporizador que controla el mapa que contiene los movimientos del personaje
    timer_mapMov=new QTimer(this);
    timer_mapMov->stop();
    connect(timer_mapMov,SIGNAL(timeout()),this,SLOT(moverObjetos()));
    timer_mapMov->start(20);

    //Crea una lista donde se introduciran las bombas
    bolas.append(new bomba());  //Creo una nueva bola al iniciar el programa por defecto
    scene->addItem(bolas.last());
    bolas.last();

    //Lineas que delimitan la escena para los objetos
    l2=new QGraphicsLineItem(0,0,0,600);    //Linea Izquierda
    l3=new QGraphicsLineItem(1000,0,1000,600);  //Linea derecha
    l4=new QGraphicsLineItem(0,500,1000,500);   //Linea abajo
    scene->addItem(l2);
    scene->addItem(l3);
    scene->addItem(l4);

    //Se crea el enemigo en el juego y se agrega a la escena
    enemigo=new enemy();
    enemigo->posicion(400);
    scene->addItem(enemigo);
    scene->setFocusItem(enemigo);

    //Numero que define que personaje se le asignara al jugador
    int numPersonaje= rand() %4 +1;

    //Crea el personaje y lo agrega a la escena
    c=new personajegraf(numPersonaje);

    c->get_personaje()->set_valores(0,0);
    c->posicion(500);
    scene->addItem(c);
    scene->setFocusItem(c);
    vel=6;  //Velocidad del personaje

    //Temporizador encargado de generar el salto del jugador
    timer_salto=new QTimer(this);
    timer_salto->stop();
    connect(timer_salto,SIGNAL(timeout()),this,SLOT(saltar()));

    //Coloca de color blanco los labels que se encuentran en el GUI
    ui->score->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));
    ui->life->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));
    ui->usernameC1->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));
    ui->game_overtxt->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));

    //Temporizador que se encarga de que caigan los corazones
    timer_lifes=new QTimer(this);
    timer_lifes->stop();
    connect(timer_lifes,SIGNAL(timeout()),this,SLOT(animar_perk()));

    //Lee el archivo de texto que contiene el nombre de usuario del jugador y su puntaje
    ifstream sesion("sesion.txt");
    string palabra="";
    if (sesion.is_open())
    {
        while (sesion.good())
        {
            char caracter;
            while (sesion.get(caracter)){
                if (caracter != ';' && caracter != '\n' && caracter !=':')
                {
                    palabra+=caracter;
                }
                if (caracter == ';'){
                    username1=palabra;
                    QString usernameQString=QString::fromStdString(palabra);
                    ui->usernameC1->setText(usernameQString);
                    palabra="";
                }
                if (caracter == ':'){
                    if (palabra == "1")
                        loadScore=true;
                    else if(palabra =="0")
                        loadScore=false;
                    palabra="";
                }
                if (caracter == '\n'){
                    if (loadScore)
                        puntaje=stoi(palabra);
                    palabra="";
                }
            }
        }
    }
    QString puntajeString;
    //Coloca en pantalla el puntaje y las vidas disponibles
    ui->score->setText("Puntaje: "+puntajeString.setNum(puntaje));
    ui->life->setText("Vidas: 3");
    //Si el puntaje sobrepasa los 50, se cambiara la imagen de fondo
    if(puntaje >= 50)
        ui->graphicsView->setBackgroundBrush(QImage(":/images/destroyed-city.jpg"));

    //Si el puntaje es cargado, se cambiará la dificultad de acuerdo a dicho puntaje
    if (puntaje <= 279)
        agregar=agregar-puntaje*10;
    else
        agregar=200;

    //Se limpian los datos por si acaso
    datosUsuario.clear();

    //Se lee el archivo save.txt para obtener los usuarios y sus puntajes para luego poder ser reemplazado
    ifstream archivo("save.txt");
    string user="";
    int score;
    palabra="";

    if (archivo.is_open())
    {
        while (archivo.good())
        {
            char c;
            while (archivo.get(c))    //ciclo para obtener cada caracter
                if (c != '\t' && c!= '\n' )
                    palabra+=c;
                else if (c== '\t')
                {
                        user=palabra;
                        palabra="";
                }
                else if(c=='\n')
                {
                    score=stoi(palabra);
                    palabra="";
                    datosUsuario.emplace(user,score);

                }
        }
    }
    //Temporizador que controla lo que sucede cuando el jugador pierde
    timer_gover=new QTimer(this);
    timer_gover->stop();
    connect(timer_gover,SIGNAL(timeout()),this,SLOT(game_over()));
    //Temporizador que controla lo que sucede antes de que el juego empiece
    timer_begin=new QTimer(this);
    timer_begin->stop();
    connect(timer_begin,SIGNAL(timeout()),this,SLOT(start()));

    ui->game_overtxt->setText("Salta para empezar");


}

game::~game()
{
    delete ui;
    delete scene;
}

void game::animar()
{
    if (c->get_personaje()->get_Vidas()<=0)
    {
        c->murio(); //Funcion que cambia la apariencia del perosnaje al de fantasma
        map<string,int>::iterator i;
        i=datosUsuario.find(username1);
        if (puntaje > i->second)    //Verifica si se supero el record
            i->second=puntaje;      //Si se supero, se reemplaza
        cargarDatos(datosUsuario);  //Coloca en el archivo de texto el nuevo contenido
        timer_gover->start(1000);
        timer->stop();
    }

    tiempoLateral += 15;
    if (tiempoLateral >= agregarLateral || agregarLateral <= 1000)
    {
        flag = true;
        lateral.append(new bomba(flag));
        scene->addItem(lateral.last());
        lateral.last();
        puntaje+=1;
        tiempoLateral = 0;
        if (agregarLateral > 1000)
            agregarLateral -= 20;
    }
    for(int j=0; j<lateral.length(); j++)
    {
        if (lateral.at(j)->salidaLateral() == 0)
        {
            if (lateral.at(j)->collidesWithItem(l3))
                lateral.at(j)->sacarDePantalla();
        }
        if (lateral.at(j)->salidaLateral() == 1)
        {
            if(lateral.at(j)->collidesWithItem(l2))
                lateral.at(j)->sacarDePantalla();
        }
        if (lateral.at(j)->collidesWithItem(c) && lateral.at(j)->explodeState())
        {
            c->get_personaje()->perderVida();
            lateral.at(j)->sacarDePantalla();
        }
        lateral.at(j)->moverLateral();
    }
    if (tiempo >= agregar || agregar == 200)
    {
        flag = false;
        bolas.append(new bomba());
        scene->addItem(bolas.last());
        bolas.last();
        //Si la bomba fue ubicada despues de los 500px, el enemigo mirara a la derecha
        if (bolas.last()->get_px()>= 500)
            enemigo->movRight();
        else //Sino, a la izquierda
            enemigo->movLeft();
        enemigo->posicion(bolas.last()->get_px());  //Coloca al enemigo en dicha posicion

        puntaje+=1;
        //Si el puntaje supera los 50 puntos, se cambiara la imagen de fondo
        if (puntaje>=50)
            ui->graphicsView->setBackgroundBrush(QImage(":/images/destroyed-city.jpg"));
        QString puntajeString, vidaString;
        puntajeString.setNum(puntaje);
        vidaString.setNum(c->get_personaje()->get_Vidas());
        //Se actualizan los puntajes y vida que se mostraran en pantalla
        ui->score->setText("Puntaje: "+puntajeString);
        ui->life->setText("Vidas: "+vidaString);
        if (agregar > 200)
            agregar -= 10;
        tiempo = 0;
        vidas = false;
    }    
    for(int i=0; i<bolas.length();i++)  //Se iterara por las bombas
    {
        if(!bolas.at(i)->collidingItems().empty())  //Si se encuentra colisionando
        {
            if (bolas.at(i)->collidesWithItem(l4))  //Si colisiona con el suelo
                bolas.at(i)->choque();  //Se realiza el rebote
            if(bolas.at(i)->collidesWithItem(l2)|| bolas.at(i)->collidesWithItem(l3))   //Si colisiona con las paredes
                bolas.at(i)->choquePared(); //Rebotara hacia el lado contrario
            if (bolas.at(i)->collidesWithItem(c) && bolas.at(i)->explodeState())    //Si exploto y colisiona con un jugador
            {
                if (vidas == false)
                    c->get_personaje()->perderVida();   //El jugador pierde vidas
                else
                    bolas.at(i)->sacarDePantalla(); //Despues se saca de pantalla
                vidas = bolas.at(i)->quitarvida();
//                    effect->setMedia(QUrl("qrc:/new/sounds/sounds/Player_Hit_0.wav"));
//                    effect->play();
            }
        }
        bolas.at(i)->mover();   //Las bolas haran el movimiento de acuerdo a su fisica
    }
    tiempo += 15;
}

void game::animar_perk()
{
    //Se repite el codigo de las bombas pero se aplica en los corazones
    tiempo2 += 15;
    if (tiempo2 >= 18000)
    {
        perk.append(new perks());
        scene->addItem(perk.last());
        perk.last();
        tiempo2 = 0;
    }
    QString puntajeString, vidaString;
    puntajeString.setNum(puntaje);
    vidaString.setNum(c->get_personaje()->get_Vidas());
    ui->score->setText("Puntaje: "+puntajeString);
    ui->life->setText("Vidas: "+vidaString);
    if (perk.length() != 0)
    {
        for(int i=0; i<perk.length();i++)
        {
            if(!perk.at(i)->collidingItems().empty())
            {
                if (perk.at(i)->collidesWithItem(l4))
                {
                    perk.at(i)->choque();
                }
                if(perk.at(i)->collidesWithItem(l2)|| perk.at(i)->collidesWithItem(l3))
                    perk.at(i)->choquePared();
                if (perk.at(i)->collidesWithItem(c) && perk.at(i)->pickableState())
                {
                    c->get_personaje()->ganarVida();
//                    effect->stop();
//                    effect->setMedia(QUrl("qrc:/sounds/get_Health.mp3"));
//                    effect->play();
                    perk.at(i)->setPos(-1000,-1000);
                }
            }
            perk.at(i)->mover();
        }
    }
}

void game::keyPressEvent(QKeyEvent *event)
{       //Funciones de teclas que activan las funciones del mapa de movimiento
    if(event->key()==Qt::Key_F4) close();
    if(event->key()== Qt::Key_D && c->get_personaje()->get_px()<730/*-(c->get_personaje()->get_w()*c->get_escala())*/)
    {   //Se mueve a la derecha
        if(keyGeneral){ //No permite mover a los lados al jugador hasta que salte para iniciar
        movimiento.at('r')=true;
        event->accept();
        }
    }
    if(event->key()== Qt::Key_A && c->get_personaje()->get_px()>-200)
    {   //Se mueve a la izquierda
        if (keyGeneral){
        movimiento.at('l')=true;
        event->accept();
        }
    }
    if(event->key() == Qt::Key_W)
    {
        if (!keyGeneral){   //Si el jugador salta y no se ha activado el juego
            keyGeneral=true;    //El juego se empezará
            ui->game_overtxt->setText("");  //Elimina el texto del centro
            timer_begin->start(800);
        }
        //Salta
            movimiento.at('j')=true;
        if (c->collidesWithItem(l4))
        {
            //Pone el falso el bool que detecta si esta tocando el suelo
            c->dejar_suelo();
           c->get_personaje()->reiniciar(); //Reinicia los valores para que toque el suelo al caer
        }
        event->accept();

    }
    if (event->key() == Qt::Key_M){
        //Conecta el arduino
        on_connect_clicked();
        event->accept();
    }
    c->posicion(500);
}
void game::keyReleaseEvent(QKeyEvent *event){       //Funciones de teclas
    if(event->key()== Qt::Key_D)
    {
        //Deja de moverse a la derecha
        movimiento.at('r')=false;
        event->accept();
    }
    if(event->key()== Qt::Key_A)
    {
        //Deja de moverse a la izquierda
        movimiento.at('l')=false;
        event->accept();

    }
    if(event->key() == Qt::Key_W)
    {   //Deja de saltar
            movimiento.at('j')=false;
            event->accept();
    }
    c->posicion(500);

}
void game::cargarDatos(map <string,int> mapaDatos){
    //Itera en el mapa de los usuario y puntajes para guardarlo en el archivo save.txt
    ofstream archivo("save.txt");
    map <string,int> :: iterator i;
    for (i=mapaDatos.begin();i!=mapaDatos.end();i++){
        archivo<<i->first<<'\t'<<i->second<<'\n';
    }
    archivo.close();
}

void game::game_over()
//Coloca en pantalla el Aviso de que el jugador ha perdido
{
//    effect->stop();
//    effect->setMedia(QUrl("qrc:/sounds/Player_Killed.wav"));
//    effect->play();
    if (cont >=0 && cont <=5){
        ui->game_overtxt->setText("Has perdido");
    }
    else if (cont >5)
        this->close();
    cont++;
}

void game::start()
{
    //Coloca en pantalla el contador hasta que la partida empieza
    if (contBegin ==0){
        ui->game_overtxt->setText("Are you ready?");
    }
    else if(contBegin ==1)
        ui->game_overtxt->setText("3");
    else if(contBegin ==2)
        ui->game_overtxt->setText("2");
    else if (contBegin==3)
        ui->game_overtxt->setText("1");
    else if(contBegin==4){
        ui->game_overtxt->setText("GO!");
        timer->start(15);
        timer_lifes->start(15);
    }
    else if(contBegin==5){
        ui->game_overtxt->setText("");
        timer_begin->stop();
    }
    contBegin++;

}

void game::saltar()
{
    //Realiza la funcion del salto
    c->jump();
    //Va actualizando la posicion en Y del jugador
    c->get_personaje()->set_py(c->get_personaje()->get_py());
    //Pone el bool del suelo si lo esta tocando para cambiar el personaje
    if (c->get_personaje()->get_tocarSuelo())
        c->tocar_suelo();
}

void game::moverObjetos()
{
    //De acuerdo al valor del mapa, se realizara la funcion del movimiento
    if (movimiento.at('r')==true)
        rightMove();
    if (movimiento.at('l')==true)
        leftMove();
    if (movimiento.at('j')==true)
        jumpMove();
}

void game::leftMove()
{
    //Se mueve a la izquierda y evita que colisione con la pared izquierda
    c->movLeft();
    if (!c->collidesWithItem(l2))
        c->get_personaje()->set_px(c->get_personaje()->get_px()-vel);
}

void game::rightMove()
{
    //Se mueve a la derecha y evita que colisione con la pared derecha
    c->movRight();
    if (!c->collidesWithItem(l3))
        c->get_personaje()->set_px(c->get_personaje()->get_px()+vel);
}

void game::jumpMove()
{
    //Inicia el timer que realiza el salto del jugador
    timer_salto->stop();
    timer_salto->start(15);
}

void game::Joy()
{
    //Detecta lo que envia el arduino para detectar que puede hacer
    char data;
    qint64 l;
       if(control->waitForReadyRead(100)){

            //Data was returned
            l = control->read(&data,1);
            switch (data) {
            case 'A':
                movimiento.at('j')=true;
            if (c->collidesWithItem(l4))
            {
                c->dejar_suelo();
               c->get_personaje()->reiniciar();

            }
                break;
            case 'B':
                movimiento.at('r')=true;
                break;
            case 'D':
                movimiento.at('l')=true;
                break;
            case 'K':
                timer_control->stop();
                control->close();
                //close();
                break;
            case 'N':
                movimiento.at('r')=false;
                movimiento.at('l')=false;
                movimiento.at('j')=false;
                break;
            default:
                movimiento.at('r')=false;
                movimiento.at('l')=false;
                movimiento.at('j')=false;
                break;
            }
            c->posicion(500);
            qDebug()<<"Response: "<<data;
            flag=false;

        }else{
            //No data
            qDebug()<<"Time out";
      }
}

void game::on_connect_clicked()
{
    control->setPortName("COM4");
    if(control->open(QIODevice::ReadWrite)){
        //Ahora el puerto seria está abierto
        if(!control->setBaudRate(QSerialPort::Baud9600)) //Configurar la tasa de baudios
            qDebug()<<control->errorString();

        if(!control->setDataBits(QSerialPort::Data8))
            qDebug()<<control->errorString();

        if(!control->setParity(QSerialPort::NoParity))
            qDebug()<<control->errorString();

        if(!control->setStopBits(QSerialPort::OneStop))
            qDebug()<<control->errorString();

        if(!control->setFlowControl(QSerialPort::NoFlowControl))
            qDebug()<<control->errorString();
            timer_control->start(20);
    }
    else{
        qDebug()<<"Serial COM4 not opened. Error: "<<control->errorString();
    }
}
