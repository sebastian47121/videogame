#ifndef MULTIPLAYER_H
#define MULTIPLAYER_H

#include <QWidget>

#include <QWidget>
#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsLineItem>
#include <QKeyEvent>
#include <QGraphicsView>
#include <QTimer>
#include "bomba.h"
#include "perks.h"
#include "personaje.h"
#include "personajegraf.h"
#include "enemy.h"
#include <QMediaPlayer>
#include <map>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

using namespace std;

namespace Ui {
class multiplayer;
}

class multiplayer : public QWidget
{
    Q_OBJECT

public:
    explicit multiplayer(QWidget *parent = nullptr);
    ~multiplayer();
public slots:
    void animar();
    void animar_perk();
    void game_over();
    void start();
    void saltar1();
    void moverObjetos();
    void leftMove1();
    void rightMove1();
    void jumpMove1();
    void saltar2();
    void leftMove2();
    void rightMove2();
    void jumpMove2();
    void Joy();
private slots:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    void on_connect_clicked();

private:
    Ui::multiplayer *ui;
    QGraphicsScene* scene;

    QGraphicsLineItem* l2;
    QGraphicsLineItem* l3;
    QGraphicsLineItem* l4;

    QList<bomba*> bolas;
    QList<perks*> perk;
    QList<bomba*> lateral;

    QTimer *timer;
    int puntaje=100;
    int agregar = 3000, tiempo = 0, tiempo2=0, agregarLateral = 2000;

    int vel;
    double dt = 0.1;
    bool vidas1 = false, vidas2=false;
    int cont=0, contBegin=0, tiempoLateral = 0;
    bool flag = false;

    string username1;
    map <string,int> datosUsuario;
    map <char, bool> movimiento1;
    map <char, bool> movimiento2;


    personajegraf *c1;
    personajegraf *c2;
    enemy *enemigo;

    QTimer *timer_mov;
    QTimer *timer_par;
    QTimer *timer_lifes;
    QTimer *timer_gover;
    QTimer *timer_begin;
    QTimer *timer_mapMov1;
    QTimer *timer_mapMov2;
    QTimer *timer_salto1;
    QTimer *timer_salto2;


    QTimer *timer_control;
    QSerialPort *control;

    QMediaPlayer *effect;

    bool keyGeneral=false, key1=false, key2=false;

};

#endif // MULTIPLAYER_H
