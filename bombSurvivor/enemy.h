#ifndef ENEMY_H
#define ENEMY_H


#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>


class enemy:public QGraphicsItem

{
public:
    enemy();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void posicion(double v_lim);
    void movRight();
    void movLeft();

private:
    double posX = 0;
    bool derecha=false;
};
#endif // ENEMY_H
