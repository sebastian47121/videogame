#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#include <QWidget>

namespace Ui {
class instructions;
}

class instructions : public QWidget
{
    Q_OBJECT

public:
    explicit instructions(QWidget *parent = nullptr);
    ~instructions();

private slots:
    void on_back_button_clicked();

private:
    Ui::instructions *ui;
};

#endif // INSTRUCTIONS_H
