#ifndef PERKS_H
#define PERKS_H
#include <QGraphicsItem>
#include <QPainter>


class perks: public QGraphicsItem
{
public:
    perks();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mover();       //Funcion que mueve el corazon
    void choque();      //Llamada cuando se detecta caida al suelo
    void choquePared();     //Se activa cuando detecta choques con una pared lateral
    bool pickableState(){return pickable;}      //Dependiendo lo que devuleva la funcion puede agregar vidas al personaje o no
private:
    double ax, ay, k, dt, g, vx, vy, masa, radio, v, angulo,posX, posY, fricSuelo;
    int aleatorio;      //Se usara para volver aleatorio el sentido en el que se mueva el corazon
    int tiempoChoque = 0;       //Se usa para controlar el tiempo que permanece el corazon en la pantalla
    int cantidad = 0;       //Usado para saber cuantas veces ha chocado con el suelo
    bool pickable=true;     //Cuuando esta true puede agregar vida por eso se inicia asi por defecto
};

#endif // PERKS_H
