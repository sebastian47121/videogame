#include "multiplayer.h"
#include "ui_multiplayer.h"
#include "mainwindow.h"
#include <QDebug>
#include <random>
#include <QMessageBox>
#include <fstream>


using namespace std;
void cargarDatos(map <string,int> &mapaDatos);

multiplayer::multiplayer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::multiplayer)
{
    ui->setupUi(this);

    QSize size(30,30);
    QPixmap pixmap(":/images/joystick.png");
    QIcon ButtonIcon(pixmap);
    ui->connect->setIcon(ButtonIcon);
    ui->connect->setIconSize(size);

    timer_control=new QTimer;
    control = new QSerialPort(this);
    connect(timer_control,SIGNAL(timeout()),this,SLOT(Joy()));

    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setFixedSize(1000,500);
    scene=new QGraphicsScene(0,0,1000,500);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setBackgroundBrush(QImage(":/images/city.jpg"));


    movimiento1.emplace('r',false);
    movimiento1.emplace('l',false);
    movimiento1.emplace('j',false);

    movimiento2.emplace('r',false);
    movimiento2.emplace('l',false);
    movimiento2.emplace('j',false);

    timer_mapMov1=new QTimer(this);
    timer_mapMov1->stop();
    connect(timer_mapMov1,SIGNAL(timeout()),this,SLOT(moverObjetos()));
    timer_mapMov1->start(20);

    timer_salto1=new QTimer(this);
    timer_salto1->stop();
    connect(timer_salto1,SIGNAL(timeout()),this,SLOT(saltar1()));

    timer_salto2=new QTimer(this);
    timer_salto2->stop();
    connect(timer_salto2,SIGNAL(timeout()),this,SLOT(saltar2()));

    timer=new QTimer(this);
    timer->stop();
    connect(timer,SIGNAL(timeout()),this,SLOT(animar()));
    //timer->start(15);

    bolas.append(new bomba());  //Creo una nueva bola al iniciar el programa por defecto
    scene->addItem(bolas.last());
    bolas.last();

    l2=new QGraphicsLineItem(0,0,0,600);    //Linea Izquierda
    l3=new QGraphicsLineItem(1000,0,1000,600);  //Linea derecha
    l4=new QGraphicsLineItem(0,500,1000,500);   //Linea abajo
    scene->addItem(l2);
    scene->addItem(l3);
    scene->addItem(l4);


    enemigo=new enemy();
    enemigo->posicion(400.0);
    scene->addItem(enemigo);
    scene->setFocusItem(enemigo);

    int numC1, numC2;
    numC1= rand()%4+1;
    do{
        numC2= rand()%4+1;
    }while(numC1==numC2);

    c1=new personajegraf(numC1);
    c2=new personajegraf(numC2);

    c1->get_personaje()->set_valores(0,0);
    c1->posicion(500);
    scene->addItem(c1);
    scene->setFocusItem(c1);

    c2->get_personaje()->set_valores(300,0);
    c2->posicion(500);
    scene->addItem(c2);
    scene->setFocusItem(c2);

    vel=6;

    ui->score->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));
    ui->life1->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));
    ui->life2->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));
    ui->game_overtxt->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));


    timer_lifes=new QTimer(this);
    timer_lifes->stop();
    connect(timer_lifes,SIGNAL(timeout()),this,SLOT(animar_perk()));
    //timer_lifes->start(15);


    ui->life1->setText("Vidas: 3");
    ui->life2->setText("Vidas: 3");
    ui->score->setText("Puntaje: 0");


    if(puntaje >= 50+90)
        ui->graphicsView->setBackgroundBrush(QImage(":/images/destroyed-city2.jpg"));

    timer_gover=new QTimer(this);
    timer_gover->stop();
    connect(timer_gover,SIGNAL(timeout()),this,SLOT(game_over()));
    timer_begin=new QTimer(this);
    timer_begin->stop();
    connect(timer_begin,SIGNAL(timeout()),this,SLOT(start()));
    //timer_begin->start(800);
    ui->game_overtxt->setText("Salten para empezar");


}

multiplayer::~multiplayer()
{
    delete ui;
    delete scene;
}

void multiplayer::animar()
{

    if (c1->get_personaje()->get_Vidas()<=0)
    {
        c1->murio();
        timer_gover->start(1000);
        timer->stop();
    }
    else if (c2->get_personaje()->get_Vidas()<=0)
    {
        c2->murio();
        timer_gover->start(1000);
        timer->stop();
    }

    tiempoLateral += 15;
    if (tiempoLateral >= agregarLateral || agregarLateral <= 1000)
    {
        flag = true;
        lateral.append(new bomba(flag));
        scene->addItem(lateral.last());
        lateral.last();
        puntaje+=1;
        tiempoLateral = 0;
        if (agregarLateral > 1000)
            agregarLateral -= 20;
    }
    for(int j=0; j<lateral.length(); j++)
    {
        if (lateral.at(j)->salidaLateral() == 0)
        {
            if (lateral.at(j)->collidesWithItem(l3))
                lateral.at(j)->sacarDePantalla();
        }
        if (lateral.at(j)->salidaLateral() == 1)
        {
            if(lateral.at(j)->collidesWithItem(l2))
                lateral.at(j)->sacarDePantalla();
        }
        if (lateral.at(j)->collidesWithItem(c1) && lateral.at(j)->explodeState())
        {
            c1->get_personaje()->perderVida();
            lateral.at(j)->sacarDePantalla();
        }
        if (lateral.at(j)->collidesWithItem(c2) && lateral.at(j)->explodeState())
        {
            c2->get_personaje()->perderVida();
            lateral.at(j)->sacarDePantalla();
        }
        lateral.at(j)->moverLateral();
    }
    if (tiempo >= agregar || agregar == 400)
    {
        bolas.append(new bomba());
        scene->addItem(bolas.last());
        bolas.last();

        if (bolas.last()->get_px()>= 500)
            enemigo->movRight();
        else
            enemigo->movLeft();
        enemigo->posicion(bolas.last()->get_px());


        puntaje+=1;
        if (puntaje>=50+120)
            ui->graphicsView->setBackgroundBrush(QImage(":/images/destroyed-city2.jpg"));
        QString puntajeString, vidaString1, vidaString2;
        puntajeString.setNum(puntaje-120);
        vidaString1.setNum(c1->get_personaje()->get_Vidas());
        vidaString2.setNum(c2->get_personaje()->get_Vidas());

        ui->score->setText("Puntaje: "+puntajeString);
        ui->life1->setText("Vidas: "+vidaString1);
        ui->life2->setText("Vidas: "+vidaString2);

        if (agregar > 200)
            agregar -= 10;
        tiempo = 0;
        vidas1 = false;
        vidas2=false;
    }

    for(int i=0; i<bolas.length();i++)
    {
        if(!bolas.at(i)->collidingItems().empty())
        {
            if (bolas.at(i)->collidesWithItem(l4))
                bolas.at(i)->choque();
            if(bolas.at(i)->collidesWithItem(l2)|| bolas.at(i)->collidesWithItem(l3))
                bolas.at(i)->choquePared();
            if (bolas.at(i)->collidesWithItem(c1) && bolas.at(i)->explodeState())
            {
                if (vidas1 == false)
                    c1->get_personaje()->perderVida();
                else
                    bolas.at(i)->sacarDePantalla();
                    vidas1 = bolas.at(i)->quitarvida();
//                    effect->setMedia(QUrl("qrc:/new/sounds/sounds/Player_Hit_0.wav"));
//                    effect->play();
            }
            if (bolas.at(i)->collidesWithItem(c2) && bolas.at(i)->explodeState())
            {
                if (vidas2 == false)
                    c2->get_personaje()->perderVida();
                else
                    bolas.at(i)->sacarDePantalla();
                    vidas2 = bolas.at(i)->quitarvida();
//                    effect->setMedia(QUrl("qrc:/new/sounds/sounds/Player_Hit_0.wav"));
//                    effect->play();
            }
        }
        bolas.at(i)->mover();
    }
    tiempo += 15;
}

void multiplayer::animar_perk()
{
    tiempo2 += 15;
    if (tiempo2 >= 6000)
    {
        perk.append(new perks());
        scene->addItem(perk.last());
        perk.last();
        tiempo2 = 0;
    }
    QString puntajeString, vidaString1, vidaString2;
    puntajeString.setNum(puntaje-100);
    vidaString1.setNum(c1->get_personaje()->get_Vidas());
    vidaString2.setNum(c2->get_personaje()->get_Vidas());

    ui->score->setText("Puntaje: "+puntajeString);
    ui->life1->setText("Vidas: "+vidaString1);
    ui->life2->setText("Vidas: "+vidaString2);

    if (perk.length() != 0)
    {
        for(int i=0; i<perk.length();i++)
        {
            if(!perk.at(i)->collidingItems().empty())
            {
                if (perk.at(i)->collidesWithItem(l4))
                {
                    perk.at(i)->choque();
                }
                if(perk.at(i)->collidesWithItem(l2)|| perk.at(i)->collidesWithItem(l3))
                    perk.at(i)->choquePared();
                if (perk.at(i)->collidesWithItem(c1) && perk.at(i)->pickableState())
                {
                    c1->get_personaje()->ganarVida();
//                    effect->stop();
//                    effect->setMedia(QUrl("qrc:/sounds/get_Health.mp3"));
//                    effect->play();
                    perk.at(i)->setPos(-1000,-1000);
                }
                if (perk.at(i)->collidesWithItem(c2) && perk.at(i)->pickableState())
                {
                    c2->get_personaje()->ganarVida();
//                    effect->stop();
//                    effect->setMedia(QUrl("qrc:/sounds/get_Health.mp3"));
//                    effect->play();
                    perk.at(i)->setPos(-1000,-1000);
                }
            }
            perk.at(i)->mover();
        }
    }
}

void multiplayer::keyPressEvent(QKeyEvent *event){       //Funciones de teclas
    if(event->key()==Qt::Key_F4) close();
    if(event->key()== Qt::Key_D && c1->get_personaje()->get_px()<730/*-(c->get_personaje()->get_w()*c->get_escala())*/)
    {
        if(keyGeneral){
        movimiento1.at('r')=true;
        event->accept();
        }
    }
    if(event->key()== Qt::Key_A && c1->get_personaje()->get_px()>-200)
    {
        if(keyGeneral){
        movimiento1.at('l')=true;
        event->accept();
        }
    }
    if(event->key() == Qt::Key_W)
    {
        key1=true;  //Llave que detecta si el jugador 1 ha saltado
        if (!keyGeneral){   //Si aun no se ha empezado el juego
            if (key1 && key2){  //Si ambos jugadores han saltado
                keyGeneral=true;    //Se activa la llave para empezar el juego
                ui->game_overtxt->setText("");
                timer_begin->start(800);
                key2=false; //Desactiva la otra llave para evitar problemas
            }

        }
            movimiento1.at('j')=true;
        if (c1->collidesWithItem(l4))
        {
           c1->dejar_suelo();
           c1->get_personaje()->reiniciar();
        }
        event->accept();

    }
    if (event->key() == Qt::Key_M){
        on_connect_clicked();
        event->accept();
    }
    c1->posicion(500);

    if(event->key()== Qt::Key_J && c2->get_personaje()->get_px()<730)
    {
        if(keyGeneral){
        movimiento2.at('l')=true;
        event->accept();}
    }
    if(event->key()== Qt::Key_L && c2->get_personaje()->get_px()>-400)
    {
        if(keyGeneral){
        movimiento2.at('r')=true;
        event->accept();}

    }
    if(event->key() == Qt::Key_I)
    {
        key2=true;      //Si el jugador 2 ha saltado
        if (!keyGeneral){
            if (key1 && key2){  //Si ambos jugadores saltaron
                keyGeneral=true;//Se activa el contador del juego
                ui->game_overtxt->setText("");
                timer_begin->start(800);
                key1=false;
            }

        }
            movimiento2.at('j')=true;
        if (c2->collidesWithItem(l4))
        {
           c2->dejar_suelo();
           c2->get_personaje()->reiniciar();
        }
        event->accept();

    }
    c2->posicion(500);
    }

void multiplayer::keyReleaseEvent(QKeyEvent *event){       //Funciones de teclas
    if(event->key()== Qt::Key_D)
    {
        movimiento1.at('r')=false;
        event->accept();
    }
    if(event->key()== Qt::Key_A)
    {
        movimiento1.at('l')=false;
        event->accept();

    }
    if(event->key() == Qt::Key_W)
    {
            movimiento1.at('j')=false;
            event->accept();

    }
    c1->posicion(500);

    if(event->key()== Qt::Key_L)
    {
        movimiento2.at('r')=false;
        event->accept();
    }
    if(event->key()== Qt::Key_J)
    {
        movimiento2.at('l')=false;
        event->accept();

    }
    if(event->key() == Qt::Key_I)
    {
        movimiento2.at('j')=false;
        event->accept();

    }
    c2->posicion(500);
}

void multiplayer::game_over()
{
//        effect->setMedia(QUrl("qrc:/new/sounds/sounds/Player_Killed.wav"));
//        effect->play();

    if (cont >=0 && cont <=5){
        if(c1->get_personaje()->get_Vidas()<=0)
        ui->game_overtxt->setText("El jugador 2 gana!");
        if(c2->get_personaje()->get_Vidas()<=0)
        ui->game_overtxt->setText("El jugador 1 gana!");
    }
    else if (cont >5)
        this->close();
    cont++;
}

void multiplayer::start()
{
    if (contBegin ==0){
        ui->game_overtxt->setText("Are you ready?");
    }
    else if(contBegin ==1)
        ui->game_overtxt->setText("3");
    else if(contBegin ==2)
        ui->game_overtxt->setText("2");
    else if (contBegin==3)
        ui->game_overtxt->setText("1");
    else if(contBegin==4){
        ui->game_overtxt->setText("GO!");
        timer->start(15);
        timer_lifes->start(15);
    }
    else if(contBegin==5){
        ui->game_overtxt->setText("");
        timer_begin->stop();
    }
    contBegin++;

}
void multiplayer::saltar1()
{
    c1->jump();
    c1->get_personaje()->set_py(c1->get_personaje()->get_py());
    if (c1->get_personaje()->get_tocarSuelo())
        c1->tocar_suelo();
}

void multiplayer::moverObjetos()
{
    if (movimiento1.at('r')==true)
        rightMove1();
    if (movimiento1.at('l')==true)
        leftMove1();
    if (movimiento1.at('j')==true)
        jumpMove1();
    if (movimiento2.at('r')==true)
        rightMove2();
    if (movimiento2.at('l')==true)
        leftMove2();
    if (movimiento2.at('j')==true)
        jumpMove2();

}

void multiplayer::leftMove1()
{
    c1->movLeft();
    if (!c1->collidesWithItem(l2))
        c1->get_personaje()->set_px(c1->get_personaje()->get_px()-vel);
}

void multiplayer::rightMove1()
{
    c1->movRight();
    if (!c1->collidesWithItem(l3))
        c1->get_personaje()->set_px(c1->get_personaje()->get_px()+vel);
}

void multiplayer::jumpMove1()
{
    timer_salto1->stop();
    timer_salto1->start(15);
}


void multiplayer::saltar2()
{
    c2->jump();
    c2->get_personaje()->set_py(c2->get_personaje()->get_py());
    if (c2->get_personaje()->get_tocarSuelo())
        c2->tocar_suelo();
}

void multiplayer::leftMove2()
{
    c2->movLeft();
    if (!c2->collidesWithItem(l2))
        c2->get_personaje()->set_px(c2->get_personaje()->get_px()-vel);
}

void multiplayer::rightMove2()
{
    c2->movRight();
    if (!c2->collidesWithItem(l3))
        c2->get_personaje()->set_px(c2->get_personaje()->get_px()+vel);
}

void multiplayer::jumpMove2()
{
    timer_salto2->stop();
    timer_salto2->start(15);
}

void multiplayer::Joy()
{
    char data;
    int l;
       if(control->waitForReadyRead(100)){

            //Data was returned
            l = control->read(&data,1);
            switch (data) {
            case 'A':
                movimiento1.at('j')=true;
            if (c1->collidesWithItem(l4))
            {
               c1->dejar_suelo();
               c1->get_personaje()->reiniciar();
            }
                break;
            case 'B':
                movimiento1.at('r')=true;
                break;
            case 'D':
                movimiento1.at('l')=true;
                break;
            case 'K':
                timer_control->stop();
                control->close();
                //close();
                break;
            case 'N':
                movimiento1.at('r')=false;
                movimiento1.at('l')=false;
                movimiento1.at('j')=false;
                break;
            default:

                break;
            }
            c1->posicion(500);
            qDebug()<<"Response: "<<data;
            flag=false;

        }else{
            //No data
            qDebug()<<"Time out";
      }
}

void multiplayer::on_connect_clicked()
{
    control->setPortName("COM4");
    if(control->open(QIODevice::ReadWrite)){
        //Ahora el puerto seria está abierto
        if(!control->setBaudRate(QSerialPort::Baud9600)) //Configurar la tasa de baudios
            qDebug()<<control->errorString();

        if(!control->setDataBits(QSerialPort::Data8))
            qDebug()<<control->errorString();

        if(!control->setParity(QSerialPort::NoParity))
            qDebug()<<control->errorString();

        if(!control->setStopBits(QSerialPort::OneStop))
            qDebug()<<control->errorString();

        if(!control->setFlowControl(QSerialPort::NoFlowControl))
            qDebug()<<control->errorString();
            timer_control->start(20);
    }
    else{
        qDebug()<<"Serial COM4 not opened. Error: "<<control->errorString();
    }
}
