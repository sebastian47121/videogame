#include "bomba.h"
#include <math.h>
#include <random>
#include <time.h>
#include <QPixmap>

bomba::bomba()
{
    ax=0;
    ay=0;
    k=0.01;  //Constante de la formula física de aceleraciones
    dt=0.1;
    g=10;   //Constante de Gravedad
    fricSuelo = 0.02;  //Constante de Friccion
    vx=rand()%100;
    vy=rand()%100;
    masa=rand()%600+50;
    radio=rand()%20 +15;
    v=sqrt(pow(vx,2)+pow(vy,2));
    angulo= atan2(vy,vx);
    shuffle = rand() % 3;  //Variable que se usara con el fin de variar las posiciones en X de donde saldra la bomba
    switch (shuffle)
    {
        case 0:
        {
            posX = rand() % 290 + radio;
            break;
        }
        case 1:
        {
            posX = rand() % 330 + 330;
            break;
        }
        case 2:
        {
            posX = rand() % 300 + 630;
            break;
        }
    }
}

bomba::bomba(bool flag1)   //Funcion sobrecargada creada para las bombas que salen lateralmente
{
    g = 10;
    ax = 0;
    dt = 0.1;
    radio = 23;
    fricSuelo = 0.02;
    random = rand() % 2;  //Se usa para alternar las salidas de las bombas por la derecha o izquierda
    lateral = flag1;  //Guardo en lateral el bool para reconocer si la bomba cae o sale por los lados
    switch (random)
    {
        case 0:
        {
            vx=rand() % 15 + 10;
            PosXL = radio*2;
            break;
        }
        case 1:
        {
            vx= -(rand() % 15 + 10);
            PosXL = 1000-(radio*2);
            break;
        }
    }
}

QRectF bomba::boundingRect() const
{
    if (lateral == false)   //Si lateral es falso es porque la bomba caera del cielo
        return QRectF(posX,radio+5,2*radio,2*radio);
    else                    //De lo contrario reconocera que es una lateral
        return QRectF(PosXL, 500-(radio*2), radio*2, radio*2);
}

QRectF bomba::boundingRectExplode() const
{
    return QRectF(posX,radio+10,2*radio,2*radio);   //Usada para las explosiones
}

void bomba::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPixmap p;
    if (lateral == false)  //Las bombas laterales se diferencian en tamaños y lugares de salida por tanto es necesario verificar
    {
        if (tiempo < 2100)
        {
            p.load(":/images/normalBomb.png");
            painter->drawPixmap(boundingRect(),p,p.rect());
            explode=false;
        }
        else if (tiempo >= 2100 && tiempo < 2900)   //Despues de aprox. 2 segundos despues de salir se mostrara la explosion
        {
            p.load(":/images/explode.png");
            painter->drawPixmap(boundingRectExplode(),p,p.rect());
            explode=true;
        }
        else  //Una vez pase poco menos de un segundo en estado de explosion la bomba desaparece
        {
            painter->drawPixmap(-50, -50, 20, 20,p);
            explode=false;
        }
    }
    else  //Si es una bomba de las laterales realiza distintas acciones
    {
        if (tiempoB < 2500)  //Si el tiempoB es menor de 2.5 segundos despues de crearse graficara una bomba
        {
            p.load(":/images/red_bomb.png");
            painter->drawPixmap(boundingRect(),p,p.rect());
            explode=false;
        }
        else if(tiempoB >= 2500 && tiempo < 3000)  //La explosion de las bombas laterales dura 0.5 segundos
        {
            p.load(":/images/explode.png");
            painter->drawPixmap(boundingRect(),p,p.rect());
            explode=true;
        }
        else if (tiempoB >= 3000)  //Si la bomba ya paso mas de 3 segundos en pantalla desaparecerá
        {
            painter->drawPixmap(-50, -50, 20, 20,p);
            sacarDePantalla();
            explode=false;
        }
    }
}

void bomba::mover() //Funcion en donde se desarollaran las físicas
{
    if (flag == false)  //Si flag esta en "false" quiere decir que es una bomba que no ha chocado con el suelo
    {
        ax=-(((k*pow(v,2)*pow(radio,2))/masa)*cos(angulo));
        ay=-(((k*pow(v,2)*pow(radio,2))/masa)*sin(angulo))-g;
        vx=vx+ax*dt;
        vy=vy+ay*dt;
        setPos(x()+vx*dt+((ax*pow(dt,2))/2),y()-vy*dt+((ay*pow(dt,2))/2));
        angulo=atan2(vy,vx);
        v= sqrt(pow(vx,2)+pow(vy,2));
    }
    else  //Es una bomba que ya tocó el suelo, por lo tanto se mueve de manera distinta
    {
        if (cantidad == 1)  //Si ha tocado una sola vez el suelo realizara un movimiento parabolico
        {
            if (shuffle == 0)  //Usamos de nuevo shuffle para un aleatorio que hara que las bombas reboten para cualquiera de los 2 lados
                vx = 10;
            if (shuffle == 1)
                vx = -10;
            setPos(x()+vx*dt, y()+vy*dt+(g*(dt*dt))/2);
            vy=vy+g*dt;
        }
        if (cantidad >= 2) //Si ya toco mas de una vez el suelo entonces lo afectara la friccion con el suelo
        {
            if (shuffle == 0)
                ax = -g*fricSuelo;
            if (shuffle == 1)
                ax = g*fricSuelo;
            vx=vx+ax*dt;
            setPos(x()+vx*dt+((ax*pow(dt,2))/2), y());
        }
    }
}

void bomba::choque()  //Funcion que detecta choque con el piso
{
    flag = true;
    cantidad += 1;  //Cada vez que se detecta choque con el suelo aumentara "cantidad"
    if(cantidad == 1)
        shuffle = rand() % 2; //Solo la primera vez se realizara la eleccion para ver para que lado se dirigira la bomba
    tiempo += 15;  //tiempo aumenta segun se actualiza la escena que es cada 15 ms
}

void bomba::choquePared()
{
    v=-v;    //Cambia la direccion de la velocidad
    vx=-vx/1.1;  //Se disminuye y cambia la velocidad en X
}

bool bomba::quitarvida()
{
    quitar = true;  // Quitar esta por defecto en falso para no quitar vidas, cuando se llama la funcion es porque se ha chocado con una explosion
    return quitar;
}

void bomba::sacarDePantalla()
{
    setPos(x()+1000, y()+1000);  //Funcion que saca los items que no son necesarios de la pantalla
}

void bomba::moverLateral()  //Funcion para el movimiento de las bombas laterales
{
    if (random == 0)  //Random ya fue aleatoriamente seleccionado y nos indica desde cual lateral salio la bomba
        ax = g*fricSuelo;
    if (random == 1)
        ax = -g*fricSuelo;
    vx=vx+ax*dt;
    setPos(x()+vx*dt+((ax*pow(dt,2))/2), y());
    tiempoB += 15;
}

int bomba::salidaLateral()  //Funcion que retornará el numero aleatorio que define el sentido del movimiento
{
    return random;
}
