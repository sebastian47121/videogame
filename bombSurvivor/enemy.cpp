#include "enemy.h"

enemy::enemy()
{
    setPos(500,40);
}

QRectF enemy::boundingRect() const
{
    return QRectF(0, 40, 150, 155); //-52
}

void enemy::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPixmap persona;
    if (derecha)
        persona.load(":/images/augustoRight.png");
    else
        persona.load(":/images/augustoLeft.png");

    painter->drawPixmap(boundingRect(), persona, persona.rect());
}



void enemy::posicion(double posX)
{
    setPos(posX, 40);
}

void enemy::movRight()
{
    derecha=true;
}

void enemy::movLeft()
{
    derecha=false;
}
