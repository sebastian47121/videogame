#include "instructions.h"
#include "ui_instructions.h"
#include <QIcon>
#include <QSize>

instructions::instructions(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::instructions)
{
    ui->setupUi(this);

    //Coloca el icono del boton para regresar al menu inicial
    QSize size(50,50);
    QPixmap pixmap(":/images/arrowL.png");
    QIcon ButtonIcon(pixmap);
    ui->back_button->setIcon(ButtonIcon);
    ui->back_button->setIconSize(size);
}

instructions::~instructions()
{
    delete ui;
}

void instructions::on_back_button_clicked()
{
    this->close();
}
