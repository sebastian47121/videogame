#ifndef PERSONAJEGRAF_H
#define PERSONAJEGRAF_H
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include "personaje.h"

class personajegraf:public QGraphicsItem

{
public:
    personajegraf(int jugador);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    personaje* get_personaje();
    void posicion(float v_lim);
    ~personajegraf();
    void movRight();
    void movLeft();
    void jump();
    bool get_suelo(){return suelo;}
    void dejar_suelo(){suelo=false;}
    void tocar_suelo(){suelo=true;}
    void murio(){muerto=true;}
private:
    personaje *player1;
    bool flag=1, move = false, suelo=true;
    int cont=1, Njugador;
    double dt = 0.1, posY = 0;
    bool muerto=false;

};

#endif // PERSONAJEGRAF_H
