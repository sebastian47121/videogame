#ifndef BOMBA_H
#define BOMBA_H
#include <QGraphicsItem>
#include <QPainter>


class bomba: public QGraphicsItem
{
public:
    bomba();
    bomba(bool flag1);      //La funcion se sobrecarga para sacar bombas laterales
    QRectF boundingRect() const;
    QRectF boundingRectExplode() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mover();    //Funcion ´para mover las bombas que caen del cielo
    void choque();      //Funcion para saber cuando choca con el suelo
    void choquePared();     //Funcion para cuando choquen con una pared lateral
    bool quitarvida();      //Funcion que quitara vida al personaje
    bool explodeState() {return explode;}       //Funcion para saber cuando quita vidas el choque del jugador con la bomba
    void sacarDePantalla();     //Saca de la pantalla elementos que no son necesarios
    void moverLateral();        //Se encarga de mover las bombas laterales
    int salidaLateral();        //Nos dice de que lado salio la bomba
    double get_px(){return posX;}       //Retorna de donde salio la bomba que caerá del suelo, usado en la clase enemy
private:
    double ax, ay, k, dt, g, vx, vy, masa, radio, v, angulo,posX, fricSuelo, PosXL;
    int shuffle;   //Variable que se usara con el fin de variar las posiciones en X de donde saldra la bomba
    int cantidad = 0;   //Usado para saber como realizar las fisicas una vez las bombas hayan tocado el suelo
    int tiempo=0;   //Usado para controlar los tiempos con los que cambian las imagenes de las bombas que caen del cielo
    int tiempoB = 0;  //Usado para controlar los tiempos con los que cambian las imagenes de las bombas laterales
    int random;   //Se usa para alternar las salidas de las bombas por la derecha o izquierda
    bool flag = false;    //Se usa para saber cuando la bomba choca con el suelo
    bool quitar = false;  //Cuando sea true quitara vidas al personaje
    bool explode=false;  //Cuando explode sea "true" sera cuando el jugador podria perder vida
    bool lateral = false;  //Sirve para diferenciar bombas laterales de bombas que caen del cielo
};

#endif // BOMBA_H
