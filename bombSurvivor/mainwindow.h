#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QMediaPlayer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    std::string getUsername(){return username;}
    int getScore(){return score;}

private slots:
    void cargarSesion(bool cargar);
    void accessData(bool cargar);
    void cambiarBG();
    void on_pushButton_clicked();
    void on_usernameTxt_textChanged(const QString &arg1);

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_music_btn_clicked();

    void on_songR_clicked();


    void on_songL_clicked();
    void cambiarCancion();

    void on_pushButton_5_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::MainWindow *ui;
    int score;
    std::string username;
    QTimer *timer_bg;
    QGraphicsScene* scene;
    int contBG=0, contPause=0, contMusic=0;
    QMediaPlayer *music;
};

#endif // MAINWINDOW_H
