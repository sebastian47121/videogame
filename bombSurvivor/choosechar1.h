#ifndef CHOOSECHAR1_H
#define CHOOSECHAR1_H

#include <QWidget>
#include <map>

using namespace std;

namespace Ui {
class choosechar1;
}

class choosechar1 : public QWidget
{
    Q_OBJECT

public:
    explicit choosechar1(QWidget *parent = nullptr);
    void cambiarFoto();
    ~choosechar1();

private slots:
    void on_bRight_clicked();

    void on_bLeft_clicked();

private:
    Ui::choosechar1 *ui;

    map <int, string> characters;
    string eleccion;
    int numEleccion=1;
};

#endif // CHOOSECHAR1_H
