#include "personajegraf.h"

personajegraf::personajegraf(int jugador)
{
    player1 = new personaje;  //Se crea un nuevo objeto de la clase personaje
    Njugador = jugador;      //Servira para escoger de manera aleatoria la imagen del jugador
}

QRectF personajegraf::boundingRect() const
{
    return QRectF(200, -102, 80, 105); //-52
}

void personajegraf::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPixmap persona;
    if (Njugador == 1)
    {
        if (move == true && suelo)  //Cuando se presiona ciertas teclas se llama a una funcion que esta mas adelante y define el sentido del movimiento
            persona.load(":/images/walkR1.png");
        if (move == false && suelo)
            persona.load(":/images/walkL1.png");
        if (move == true && !suelo)
            persona.load(":/images/jumpcharR1.png");
        if (move == false && !suelo)
            persona.load(":/images/jumpcharL1.png");
        if (move == true && muerto)
            persona.load(":/images/deadR.png");
        if (move == false && muerto)
            persona.load(":/images/deadL.png");
    }
    else if (Njugador==2)
    {
        if (move == true && suelo)
            persona.load(":/images/walkR2.png");
        if (move == false && suelo)
            persona.load(":/images/walkL2.png");
        if (move == true && !suelo)
            persona.load(":/images/jumpcharR2.png");
        if (move == false && !suelo)
            persona.load(":/images/jumpcharL2.png");
        if (move == true && muerto)
            persona.load(":/images/deadR.png");
        if (move == false && muerto)
            persona.load(":/images/deadL.png");
    }
    else if (Njugador==3)
    {
        if (move == true && suelo)
            persona.load(":/images/walkR3.png");
        if (move == false && suelo)
            persona.load(":/images/walkL3.png");
        if (move == true && !suelo)
            persona.load(":/images/jumpcharR3.png");
        if (move == false && !suelo)
            persona.load(":/images/jumpcharL3.png");
        if (move == true && muerto)
            persona.load(":/images/deadR.png");
        if (move == false && muerto)
            persona.load(":/images/deadL.png");
    }
    else if (Njugador==4)
    {
        if (move == true && suelo)
            persona.load(":/images/walkR4.png");
        if (move == false && suelo)
            persona.load(":/images/walkL4.png");
        if (move == true && !suelo)
            persona.load(":/images/jumpcharR4.png");
        if (move == false && !suelo)
            persona.load(":/images/jumpcharL4.png");
        if (move == true && muerto)
            persona.load(":/images/deadR.png");
        if (move == false && muerto)
            persona.load(":/images/deadL.png");
    }
    painter->drawPixmap(boundingRect(), persona, persona.rect());
}

personaje* personajegraf::get_personaje()
{
    return player1;
}

void personajegraf::posicion(float v_lim)  //Cambiara las posiciones del jugador
{
    setPos(player1->get_px(),(v_lim-player1->get_py()));
}
void personajegraf::jump()  //Hara saltar al jugador
{
   setPos(player1->get_px(),(500-player1->get_py()));
    player1->jump();
}

personajegraf::~personajegraf()
{
    delete player1;
}

void personajegraf::movRight()
{
    move = true;  //Hara que la imagen cambie el sentido del movimiento
}

void personajegraf::movLeft()
{
    move = false;
}
