#ifndef PERSONAJE_H
#define PERSONAJE_H


class personaje
{
private:
    float px;
    float py;
    float vy;
    float G;
    double dt=0.1;
    int vidas=3;
    bool tocarSuelo=true;

public:
    personaje();
    void set_valores(float x, float y);
    float get_px();
    float get_py();

    int get_Vidas(){return vidas;}
    void perderVida();
    void ganarVida();
    void jump();
    void reiniciar(){py=0;vy=50;}

    void set_px(float x);

    void set_py(float y);
    bool get_tocarSuelo(){return tocarSuelo;}

};

#endif // PERSONAJE_H
