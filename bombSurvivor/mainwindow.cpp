#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "game.h"
#include "multiplayer.h"
#include "instructions.h"
#include <fstream>
#include <iostream>
#include <QMessageBox>
#include <QSize>
#include <QIcon>


using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Se colocan iconos en los botones de la música
    QSize size(30,30);
    QPixmap pixmap(":/images/music.png");
    QIcon ButtonIcon(pixmap);
    ui->music_btn->setIcon(ButtonIcon);
    ui->music_btn->setIconSize(size);

    QPixmap next(":/images/next.png");
    QIcon ButtonIcon2(next);
    ui->songR->setIcon(ButtonIcon2);
    ui->songR->setIconSize(size);

    QPixmap previous(":/images/previous.png");
    QIcon ButtonIcon3(previous);
    ui->songL->setIcon(ButtonIcon3);
    ui->songL->setIconSize(size);

    QIcon rectangle(":/images/rectangle.png");
    ui->rectangle->setPixmap(rectangle.pixmap(250,100));

    //Se coloca de color blanco el label con el nombre de la cancion
    ui->lbl_song->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));



    //Se elimina el Scrollbar de la ventana
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setFixedSize(1000,500);

    contBG=1;   //Contador que define la imagen de fondo

    scene=new QGraphicsScene(0,0,1000,500); //Dimensiones de la ventana
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setBackgroundBrush(QImage(":/images/bg1.jpg"));   //Se coloca la imagen inicial de fondo
    //Timer encargado de cambiar la imagen de fondo cada 5 segundos
    timer_bg=new QTimer(this);
    timer_bg->stop();
    connect(timer_bg,SIGNAL(timeout()),this,SLOT(cambiarBG()));
    timer_bg->start(5000);

    //Se reproduce una cancion de fondo
    music =new QMediaPlayer();
    music->stop();
    music->setMedia(QUrl("qrc:/sounds/M.O.O.N. - 'Crystals'.mp3"));
    music->play();



}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::cargarSesion(bool cargar)
{   //Funcion que se encarga de escribir en el archivo sesion.txt si el usuario desea cargar el puntaje
    char load='0';
    if (cargar)
        load='1';
    ofstream archivo("sesion.txt");
    archivo<<username<<';'<<load<<':'<<score<<'\n';
    archivo.close();
}

void MainWindow::on_pushButton_clicked()
{
    //Boton que inicia la partida ARCADE sin cargar ningun puntaje maximo
    username=ui->usernameTxt->text().toStdString();
    accessData(false);
    cargarSesion(false);
    //close();
    game *MyGame = new game(); MyGame->show();
}

void MainWindow::accessData(bool cargar)
{
    //Funcion que se encarga de buscar en el archivo de texto si existe el jugador y su puntaje
    ifstream archivo("save.txt");
    string palabra="";
    bool find=false;    //Bool que se cambia a true si se encuentra el nombre de usuario en el archivo


    if (archivo.is_open())
    {
        while (archivo.good())
        {
            char c;
            while (archivo.get(c))    //ciclo para obtener cada caracter
                if (c != '\t' && c!= '\n' )
                    palabra+=c;
                else if (c== '\t')
                {
                    if (palabra== ui->usernameTxt->text().toStdString())
                    {
                        find = true;
                        palabra="";
                    }
                }
                else if(c=='\n')
                {
                    if (find)
                    {
                        //Si el usuario se encontro, obtiene el puntaje que ya contiene
                        score=stoi(palabra);
                        break;
                    }
                    else
                        palabra="";
                }

        }
        if (find==true && cargar && score==0)
        {
            //Si se encontro el usuario pero no tiene puntaje maximo, se le advertira
            QMessageBox::information(this,tr("Sin datos"),tr("Este usuario no tiene datos que cargar."));
        }
        else if (find ==false && cargar==false)
        {
            //Si no se encontro el jugador, y piensa crear una nueva partida
           //Este será creado en el archivo de guardado
            archivo.close();
            ofstream archivo2;
            archivo2.open("save.txt",std::ios::app);
            score=0;
            archivo2 << username << '\t'<<'0'<<'\n';
        }
    }
    else
    {
        archivo.close();
        ofstream file("save.txt");
        score=0;
        file << username << '\t'<<'0'<<'\n';
    }
    archivo.close();
}

void MainWindow::cambiarBG()
{
    //Se cambia de color el texto del titulo de acuerdo a la imagen de fondo para que este titulo no se confunda
    if(contBG==0){
        ui->label->setStyleSheet(QStringLiteral("QLabel{color: rgb(0, 0, 0);}"));
        ui->graphicsView->setBackgroundBrush(QImage(":/images/bg1.jpg"));}
    else if(contBG==1){
        ui->label->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));
        ui->graphicsView->setBackgroundBrush(QImage(":/images/bg2.jpg"));}
    else if(contBG==2){
        ui->label->setStyleSheet(QStringLiteral("QLabel{color: rgb(240, 138, 117);}"));
        ui->graphicsView->setBackgroundBrush(QImage(":/images/bg3.jpg"));}
    else if(contBG==3)
        contBG=-1;
    contBG++;

}

void MainWindow::on_usernameTxt_textChanged(const QString &arg1)
{
    //Funcion que habilita los botones para acceder a los juegos Single player
    //Solo si se ingreso un nombre de usuario correcto, en este caso de mas de 5 caracteres
    if (ui->usernameTxt->text().size()>=5){
        ui->pushButton->setEnabled(true);
        ui->pushButton_3->setEnabled(true);
    }
    else if (ui->usernameTxt->text().size()<5){
        ui->pushButton->setDisabled(true);
        ui->pushButton_3->setDisabled(true);
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    //Funcion para iniciar la partida con el puntaje maximo como inicio
    username=ui->usernameTxt->text().toStdString();
    accessData(true);
    cargarSesion(true);
    game *MyGame = new game(); MyGame->show();
}

void MainWindow::on_pushButton_2_clicked()
{
    //Funcion para ingresar al modo de 2 Jugadores
    multiplayer *MyGame = new multiplayer(); MyGame->show();

}

void MainWindow::on_music_btn_clicked()
{
    QSize size(30,30);

    //Contador que define el icono si la musica esta pausada o no
    if(contPause==1){
        //Icono de musica reproduciendo
        QPixmap pixmap(":/images/music.png");
        QIcon ButtonIcon(pixmap);
        ui->music_btn->setIcon(ButtonIcon);
        ui->music_btn->setIconSize(size);
        music->play();
        contPause=0;
    }
    else if (contPause==0){
        //Icono de musica pausada
        QPixmap pixmap(":/images/music2.png");
        QIcon ButtonIcon(pixmap);
        ui->music_btn->setIcon(ButtonIcon);
        ui->music_btn->setIconSize(size);
        music->pause();
        contPause=1;
    }
}

void MainWindow::on_songR_clicked()
{
    //Aumenta el contador que define cual cancion se reproduce
    contMusic++;
    cambiarCancion();

}

void MainWindow::on_songL_clicked()
{
    //Reduce el contador que define cual cancion se reproduce
    contMusic--;
    cambiarCancion();
}

void MainWindow::cambiarCancion()
{
    //Dependiendo del contador que es cambiado constantemente, se cambiara la cancion
    switch(contMusic) {
        case 1 :
        music->stop();
        music->setMedia(QUrl("qrc:/sounds/M.O.O.N. - 'Hydrogen'.mp3"));
        music->play();
        ui->lbl_song->setText("Hydrogen");
                 break;
        case 2 :
        music->stop();
        music->setMedia(QUrl("qrc:/sounds/maintheme.mp3"));
        music->play();
        ui->lbl_song->setText("Sticky Situation");
                 break;
    case 0:
        music->stop();
        music->setMedia(QUrl("qrc:/sounds/M.O.O.N. - 'Crystals'.mp3"));
        music->play();
        ui->lbl_song->setText("Crystals");
        break;
    case -1:
        //Al bajar mas de lo indicado, se regresara al ultimo
        contMusic=2;
        music->stop();
        music->setMedia(QUrl("qrc:/sounds/maintheme.mp3"));
        music->play();
        ui->lbl_song->setText("Sticky Situation");
        break;
    case 3:
        //Al subir mas de lo indiciado, se regresara el primero
        contMusic=0;
        music->stop();
        music->setMedia(QUrl("qrc:/sounds/M.O.O.N. - 'Crystals'.mp3"));
        music->play();
        ui->lbl_song->setText("Crystals");
        break;
    }
}

void MainWindow::on_pushButton_5_clicked()
{
    //Cierra el juego
    close();
}

void MainWindow::on_pushButton_4_clicked()
{
    //Abre el form donde se encuentran las instrucciones
    instructions *Instrucciones = new instructions(); Instrucciones->show();
}
