#include "perks.h"
#include <math.h>
#include <random>
#include <time.h>
#include <QPixmap>

perks::perks()
{
    srand(time(NULL));
    ax=0;
    ay=0;
    k=0.01;
    dt=0.1;
    g=10;
    fricSuelo = 0.02;
    vx=rand()%100;
    vy=rand()%100;
    masa=rand()%600+50;
    radio=rand()%30 +15;
    v=sqrt(pow(vx,2)+pow(vy,2));
    angulo= atan2(vy,vx);
    posX = rand() % 800 + radio;
    posY = radio +5;
}

QRectF perks::boundingRect() const
{
    return QRectF(posX,posY,60,40);
}

void perks::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPixmap p;
    if (pickable == true)       //Aparecera un corazon normal mientras no haya chocado con el suelo
    {
        p.load(":/images/heart.png");
        painter->drawPixmap(boundingRect(),p,p.rect());
    }
    else        //Si ya choco aparecera un corazon roto
    {
        if (tiempoChoque >= 1200)       //Si el corazon roto ha durado en la pantalla mas de un minuto lo sacara de la pantalla
            painter->drawPixmap(-10000, -10000, 20, 20,p);
        else
        {
            p.load(":/images/brokenheart.png");
            painter->drawPixmap(boundingRect(),p,p.rect());
        }
        tiempoChoque += 15;
    }
}

void perks::mover()     //Funcion en donde se incluyen las fisicas del corazon
{
    if (pickable == true) //Si pickable esta true significa que no se ha chocado con el suelo
    {
        ax=-(((k*pow(v,2)*pow(radio,2))/masa)*cos(angulo));
        ay=-(((k*pow(v,2)*pow(radio,2))/masa)*sin(angulo))-g;
        vx=vx+ax*dt;
        vy=vy+ay*dt;
        posX = posX +vx*dt+((ax*pow(dt,2))/2);
        posY = posY-vy*dt+((ay*pow(dt,2))/2);
        setPos(x()+vx*dt+((ax*pow(dt,2))/2),y()-vy*dt+((ay*pow(dt,2))/2));
        angulo=atan2(vy,vx);
        v= sqrt(pow(vx,2)+pow(vy,2));
    }
    if (pickable == false)  //Cuando esta en false ya chocó
    {
        if (aleatorio == 0)     //Define el sentido del juego
            ax = -g*fricSuelo;
        if (aleatorio == 1)
            ax = g*fricSuelo;
        vx=vx+ax*dt;
        setPos(x()+vx*dt+((ax*pow(dt,2))/2),y());
    }
}

void perks::choque()
{
    if(cantidad == 0)
        aleatorio = rand() % 2;     //definirá el sentido en el que se movera el corazon luego del choque con el suelo
    cantidad += 1;
    pickable = false;       //Una vez el corazon toca el suelo no quitara vidas por cambiar el bool a false
}

void perks::choquePared()
{
    v=-v;       //Cambia la velocidad para cambiar el sentido del movimiento
    vx=-vx/1.1;
}
